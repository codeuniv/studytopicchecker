package ru.codeuniverse.lrntopicchecker.view;

public enum Importance {

	LOW, MEDIUM, HIGH;

}
