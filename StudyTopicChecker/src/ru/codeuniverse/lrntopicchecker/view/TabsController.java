package ru.codeuniverse.lrntopicchecker.view;

import java.io.InputStream;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class TabsController {

	protected static final Color IMPORTANCE_MEDIUM_COLOR = Color.DARKSLATEBLUE;

	protected static final Color IMPORTANCE_LOW_COLOR = Color.GREEN;

	protected static final int BUTTON_HEIGHT = 30; // orig: 50

	protected static final int IMAGE_SIZE_PRESSED = 20; // orig: 30

	// same for pressed / unpressed buttons
	protected static final int FONT_SIZE = 16;

	// nice one: Color.DEEPPINK Color.SLATEGREY Color.HOTPINK
	// Color.BLUEVIOLET
	protected static final Color UNPRESSED_TEXT_COLOR = Color.HOTPINK;

	// nice one: Britannic Bold (!), Stencil
	protected static final String FONT_NAME_UNPRESSED = "Arial";

	protected static final String FONT_NAME_PRESSED = "Arial";

	// nice one: Color.CORNFLOWERBLUE
	protected static final Color PRESSED_TEXT_COLOR = Color.CORNFLOWERBLUE;

	protected static final String BTN_IMG_STUDIED = "/circle_blue_checkmark1.png";

	protected static final double SPACE_BETWEEN_BUTTONS_HORIZONTALLY = 10;

	protected static final double SPACE_BETWEEN_BUTTONS_VERTICALLY = 10;

	protected static final FontWeight FONT_BOLDNESS = FontWeight.EXTRA_BOLD;

	int topicsAlreadyStudied = 0;
	IntegerProperty topicsAlreadyStudiedProperty = new SimpleIntegerProperty();

	int numberOfTopics; // number of buttons
	IntegerProperty topicsNOTStudiedProperty = new SimpleIntegerProperty();

	// applies same formatting to Tab1 and Tab2 FlowPanes
	// Tab1 and Tab2 have same design
	protected void initTargetTabFlowPane(FlowPane targetTabFlowPane) {
		targetTabFlowPane.setPadding(new Insets(30, 30, 30, 30));
		targetTabFlowPane.setHgap(SPACE_BETWEEN_BUTTONS_HORIZONTALLY);
		targetTabFlowPane.setVgap(SPACE_BETWEEN_BUTTONS_VERTICALLY);
		targetTabFlowPane.setOrientation(Orientation.VERTICAL);
	}

	void setUnpressedBtnGenericStyle(ToggleButton button) {
		Font font = Font.font(FONT_NAME_UNPRESSED, FONT_BOLDNESS, FONT_SIZE);
		button.setFont(font);
		Color textColor = UNPRESSED_TEXT_COLOR;
		button.setTextFill(textColor);
		button.setPrefHeight(BUTTON_HEIGHT);
		// Effect effect = new InnerShadow();
		// Effect effect = new Lighting();
		// button.setEffect(effect);
		// button.setUnderline(true);
		// button.setStyle("-fx-border-color: green;");
	}

	// adjust default style depending on importance of button (topic)
	void adjustUnpressedBtnStyle(ToggleButton button, Importance importance) {
		if (importance == Importance.LOW) {
			button.setTextFill(IMPORTANCE_LOW_COLOR);
		} else if (importance == Importance.MEDIUM) {
			button.setTextFill(IMPORTANCE_MEDIUM_COLOR);
		} else if (importance == Importance.HIGH) {
			// NO ACTION
		}
	}

	protected Image getImageForBtn(String imgResource) {
		InputStream is = getClass().getResourceAsStream(imgResource);
		Image img = new Image(is, IMAGE_SIZE_PRESSED, 0, true, true);
		return img;
	}

}
