package ru.codeuniverse.lrntopicchecker.view;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.HostServices;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import ru.codeuniverse.lrntopicchecker.dao.TopicDataAccess;
import ru.codeuniverse.lrntopicchecker.domain.Topic;

public class Tab2ContentController extends TabsController {

	private static final Logger logger = LoggerFactory
			.getLogger(Tab2ContentController.class);

	private static final String BTN_IMG_LINKS = "/linkImg1.png";

	private Image btnImage = getImageForBtn(BTN_IMG_LINKS);

	@FXML
	private BorderPane tab2Root;

	@FXML
	private FlowPane tab2FlowPane;

	// TODO get HostServices non-statically
	private HostServices hostServices = FXMain.hostServices;

	private TextInputDialog addLinkDialog = createAddLinkDialog();

	private List<Topic> topics;

	private TopicDataAccess topicDAO = new TopicDataAccess();
	{ // to close connection at app shutdown
		FXMain.setTopicDAO(topicDAO);
	}

	public Tab2ContentController() {
		try {
			topics = topicDAO.getAllTopics();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				topicDAO.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private TextInputDialog createAddLinkDialog() {
		addLinkDialog = new TextInputDialog();
		addLinkDialog.setTitle("Add new link");
		String headerText = "Enter web link or path to file/folder";
		addLinkDialog.setHeaderText(headerText);
		addLinkDialog.setContentText(null);
		// add icon to dialog wnd
		addIconToDialogWnd(addLinkDialog);

		return addLinkDialog;
	}

	private void addIconToDialogWnd(TextInputDialog addLinkDialog) {
		InputStream dlgWndImgStream = getClass()
				.getResourceAsStream("/circle_blue_checkmark1.png");
		Image dlgImg = new Image(dlgWndImgStream);

		// get Stage of dialog window
		Stage dlgStage = (Stage) addLinkDialog.getDialogPane().getScene()
				.getWindow();
		dlgStage.getIcons().add(dlgImg);
	}

	private EventHandler<ActionEvent> btnClickHandler = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			String btnID = ((ToggleButton) event.getSource()).getId();
			Topic topic = topics.get(Integer.parseInt(btnID) - 1);
			List<String> links = topic.getTopicUrl();
			if (links == null) {
				return;
			}
			for (String linkToShow : links) {
				hostServices.showDocument(linkToShow);
			}
		}
	};

	private void createAddAndStyleTab_2_Buttons() {
		// 1) add buttons programmatically
		// 2) apply style depending on importance of button (topic)
		for (Topic topic : topics) {
			ToggleButton button = new ToggleButton(topic.getTopicName());
			tab2FlowPane.getChildren().add(button);
			setUnpressedBtnGenericStyle(button);
			adjustUnpressedBtnStyle(button, topic.getTopicImportance());
			button.setMaxWidth(Double.MAX_VALUE);
			button.setId(String.valueOf(topic.getTopicNumber()));
			addContextMenu(button);
		}
	}

	private void initButtons() {
		int numOfChildren = tab2FlowPane.getChildren().size();
		numberOfTopics = numOfChildren;
		for (int i = 0; i < numberOfTopics; i++) {
			ToggleButton button = (ToggleButton) tab2FlowPane.getChildren()
					.get(i);
			button.setOnAction(btnClickHandler);
			// TODO add img to all buttons (looks better w/o img thou!)
			// ImageView btnImageView = new ImageView(btnImage);
			// button.setGraphic(btnImageView);
			// }
		}
	}

	private void addContextMenu(ToggleButton targetButton) {
		ContextMenu contextMenu = new ContextMenu();
		MenuItem addLink_MenuItem = new MenuItem("Add Link");
		MenuItem showAllLinks__MenuItem = new MenuItem("Show All Links");
		contextMenu.getItems().add(addLink_MenuItem);
		contextMenu.getItems().add(showAllLinks__MenuItem);

		addLink_MenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				// show input dialog window
				Optional<String> dlgResult = addLinkDialog.showAndWait();
				// if user input no text, return and do nothing
				if (!dlgResult.isPresent()) {
					return;
				}
				if (dlgResult.get().isEmpty()) {
					return;
				}
				// if user input text, add it to database
				String btnID = targetButton.getId();
				int topicID = Integer.parseInt(btnID);
				String Url = dlgResult.get();
				// TODO chk if url / filepath is valid
				topicDAO.insertStudyAidURL(topicID, Url);
			}
		});
		targetButton.setContextMenu(contextMenu);
	}

	@FXML
	void initialize() {
		initTargetTabFlowPane(tab2FlowPane);
		createAddAndStyleTab_2_Buttons();
		initButtons();
	}

}
