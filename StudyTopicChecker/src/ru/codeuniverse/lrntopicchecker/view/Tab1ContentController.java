package ru.codeuniverse.lrntopicchecker.view;

import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Tab1ContentController extends TabsController {

	@FXML
	private BorderPane tab1Root;

	@FXML
	FlowPane tab1FlowPane;

	private Image btnImage = getImageForBtn(BTN_IMG_STUDIED);

	// generic btn click event handler for all buttons
	private EventHandler<ActionEvent> btnClickHandler = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			// if any checkbox within botton clicked, do nothing
			if (event.getTarget() instanceof CheckBox) {
				return;
			}
			// if Button itself is clicked, process it
			if (event.getSource() instanceof ToggleButton) {
				// add image to button and change its style
				ImageView btnImageView = new ImageView(btnImage);
				((ToggleButton) event.getSource()).setGraphic(btnImageView);
				setPressedBtnStyle((ToggleButton) event.getSource());
				// increase topics-studied property
				topicsAlreadyStudiedProperty.set(++topicsAlreadyStudied);
				// calculate topics-not-studied property
				topicsNOTStudiedProperty.set(numberOfTopics - topicsAlreadyStudied);
				// image above text
				// btn1.setContentDisplay(ContentDisplay.TOP);
				// distance from img to text
				// btn1.setGraphicTextGap(7.0);
			}
		}
	};

	protected void createAddAndStyleTab_1_Buttons() {
		// 1) add buttons programmatically
		// 2) apply style depending on importance of button (topic)
		ButtonMetaInfoFactory btnMetaInfoFactory = new ButtonMetaInfoFactory();
		List<ButtonMetaInfo> btnMetaInfoList = btnMetaInfoFactory.getList();
		for (ButtonMetaInfo btnMetaInfo : btnMetaInfoList) {
			ToggleButton button = new ToggleButton(btnMetaInfo.getText());
			tab1FlowPane.getChildren().add(button);
			setUnpressedBtnGenericStyle(button);
			adjustUnpressedBtnStyle(button, btnMetaInfo.getImportance());
			button.setMaxWidth(Double.MAX_VALUE);
		}
	}

	private void setPressedBtnStyle(ToggleButton button) {
		Font font = Font.font(FONT_NAME_PRESSED, FONT_BOLDNESS, FONT_SIZE);
		button.setFont(font);
		Color textColor = PRESSED_TEXT_COLOR;
		button.setTextFill(textColor);
		button.setPrefHeight(BUTTON_HEIGHT);
	}

	private void initButtons() {
		int numOfChildren = tab1FlowPane.getChildren().size();
		numberOfTopics = numOfChildren;
		for (int i = 0; i < numberOfTopics; i++) {
			if (tab1FlowPane.getChildren().get(i) instanceof ToggleButton) {
				ToggleButton button = (ToggleButton) tab1FlowPane.getChildren().get(i);
				// add button-click handler
				button.addEventHandler(ActionEvent.ACTION, btnClickHandler);
				// add 1 or 2 checkboxes depending on button (topic) importance
				if (i < 11) {
					addTwoChkBoxesToButton(button);
				} else {
					addOneChkBoxToButton(button);
				}
			}
		}
	}

	private void addOneChkBoxToButton(ToggleButton button) {
		// add pomodoro chkbox 1
		CheckBox chkbx1 = new CheckBox();
		chkbx1.setOnMouseClicked((e) -> {
			chkbx1.setSelected(true);
			e.consume();
		});
		button.setGraphic(chkbx1);
		button.setContentDisplay(ContentDisplay.RIGHT);
		button.setGraphicTextGap(8);
	}

	private void addTwoChkBoxesToButton(ToggleButton button) {
		// add pomodoro chkbox 1
		CheckBox chkbx1 = new CheckBox();
		chkbx1.setOnMouseClicked((e) -> {
			chkbx1.setSelected(true);
			e.consume();
		});
		// add pomodoro chkbox 2
		CheckBox chkbx2 = new CheckBox();
		chkbx2.setOnMouseClicked((e) -> {
			chkbx2.setSelected(true);
			e.consume();
		});
		HBox hbox = new HBox();
		hbox.getChildren().addAll(chkbx1, chkbx2);
		button.setGraphic(hbox);
		button.setContentDisplay(ContentDisplay.RIGHT);
		button.setGraphicTextGap(8);
	}

	@FXML
	private void initialize() {
		initTargetTabFlowPane(tab1FlowPane);
		createAddAndStyleTab_1_Buttons();
		initButtons();
	}

}
