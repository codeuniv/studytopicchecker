package ru.codeuniverse.lrntopicchecker.view;

import org.controlsfx.control.StatusBar;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.util.converter.NumberStringConverter;

public class MainViewController {

	@FXML
	private Tab tab1;

	@FXML
	private Tab1ContentController tab1ContentController;

	// "Links Wizard"
	private static final String TAB2_TITLE = "LINKS WIZARD";
	// "Topic Tracker"
	private static final String TAB1_TITLE = "TOPIC TRACKER";
	// "Topic Tracker"
	private static final String TAB3_TITLE = "PROGRESS STATS";

	@FXML
	private BorderPane sceneRootPane;

	@FXML
	private TabPane tabsContainer;

	@FXML
	private Tab tab2;

	private StatusBar statusBar;

	@FXML
	private void initialize() {
		initTabsContainer();
		addStatusBar();
		initStatusBar();
	}

	private void initTabsContainer() {
		tab1.setText(TAB1_TITLE);
		tab2.setText(TAB2_TITLE);
		// tab3.setText(TAB1_TITLE);
	}

	private void addStatusBar() {
		statusBar = new StatusBar();
		sceneRootPane.setBottom(statusBar);
	}

	private void initStatusBar() {
		statusBar.setText(null);
		System.out.println("tab1Controller: " + tab1ContentController);
		int nButtons = tab1ContentController.tab1FlowPane.getChildren().size();
		Label numOfTopics = new Label(); // equals to num of displayed buttons

		numOfTopics.setText("Total topics: " + nButtons);
		statusBar.getLeftItems().add(numOfTopics);

		Label numOfStudiedTopicsTitle = new Label();
		numOfStudiedTopicsTitle.setText(" Already Studied: ");
		statusBar.getLeftItems().add(numOfStudiedTopicsTitle);

		Label numOfStudiedTopicsValue = new Label();
		Bindings.bindBidirectional(numOfStudiedTopicsValue.textProperty(),
				tab1ContentController.topicsAlreadyStudiedProperty, new NumberStringConverter());
		statusBar.getLeftItems().add(numOfStudiedTopicsValue);

		Label numOfNOTStudiedTopicsTitle = new Label();
		numOfNOTStudiedTopicsTitle.setText(" NOT STUDIED: ");
		statusBar.getLeftItems().add(numOfNOTStudiedTopicsTitle);

		Label numOfNOTStudiedTopicsValue = new Label();
		Bindings.bindBidirectional(numOfNOTStudiedTopicsValue.textProperty(),
				tab1ContentController.topicsNOTStudiedProperty, new NumberStringConverter());
		statusBar.getLeftItems().add(numOfNOTStudiedTopicsValue);
	}
}
