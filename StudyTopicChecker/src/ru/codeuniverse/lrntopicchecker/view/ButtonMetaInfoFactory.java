package ru.codeuniverse.lrntopicchecker.view;

import java.util.ArrayList;
import java.util.List;

//DELME del class
//ALL LINKS ARE ADDED DIRECTLY TO DATABASE!!!
public class ButtonMetaInfoFactory {

	private List<ButtonMetaInfo> btnMetaInfoList;

	private List<String> links;

	public ButtonMetaInfoFactory() {
		btnMetaInfoList = new ArrayList<ButtonMetaInfo>();
		links = new ArrayList<>();

		// REPEAT
		String text = "ВЧЕРА Audio";
		Importance importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo1 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo1);

		text = "Mnemosyne NEW";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo2 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo2);

		text = "Mnemosyne OLD";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo3 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo3);

		text = "Cram / Important";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo4 = new ButtonMetaInfo(text, importance);
		links.clear();
		links.add("P:\\BOOKS_READ_r+\\АЛГОРИТМЫ\\Big-O Algorithm Complexity Cheat Sheet.html");
		System.out.println("links in ctor:" + links);
		btnMetaInfo4.setLinks(links);
		btnMetaInfoList.add(btnMetaInfo4);
		System.out.println("ctor: btnMetaInfoList[3]: " + btnMetaInfoList.get(3).getLinks());

		text = "Конспекты";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo5 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo5);

		text = "Старое Аудио";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo6 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo6);

		// STUDY
		text = "Patterns";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo7 = new ButtonMetaInfo(text, importance);
		// links
		links.clear();
		links.add("https://en.wikipedia.org/wiki/Category:Software_design_patterns");
		links.add("http://stackoverflow.com/documentation/design-patterns/topics");
		links.add("http://java-design-patterns.com/patterns/");
		links.add("P:\\BOOKS_READ_r+\\patterns");
		btnMetaInfo7.setLinks(links);
		btnMetaInfoList.add(btnMetaInfo7);

		// PROG TASKS
		text = "Prog Tasks";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo8 = new ButtonMetaInfo(text, importance);
		// links
		links.clear();
		links.add("http://www.java2s.com/Code/Java/CatalogJava.htm");
		btnMetaInfo8.setLinks(links);
		btnMetaInfoList.add(btnMetaInfo8);

		// JLS
		text = "JLS";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo9 = new ButtonMetaInfo(text, importance);
		// links
		links.clear();
		links.add("P:\\p66__Java LAng Spec__SE8.pdf");
		btnMetaInfo9.setLinks(links);
		btnMetaInfoList.add(btnMetaInfo9);

		text = "Core API";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo10 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo10);

		text = "Certification";
		importance = Importance.HIGH;
		ButtonMetaInfo btnMetaInfo11 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo11);

		text = "Tutorials";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo12 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo12);

		text = "Tests";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo13 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo13);

		text = "Hackerrank";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo14 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo14);

		text = "Interview Q&A";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo15 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo15);

		text = "Algorithms";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo16 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo16);

		text = "Own Project";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo17 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo17);

		text = "JUnit/Hamcrest";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo18 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo18);

		text = "Logging";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo19 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo19);

		text = "Mockito";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo20 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo20);

		text = "SQL";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo21 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo21);

		text = "Git";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo22 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo22);

		text = "Maven/Gradle";
		importance = Importance.MEDIUM;
		ButtonMetaInfo btnMetaInfo23 = new ButtonMetaInfo(text, importance);
		btnMetaInfoList.add(btnMetaInfo23);

		text = "Libs";
		ButtonMetaInfo btnMetaInfo24 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo24);

		text = "Linux";
		ButtonMetaInfo btnMetaInfo25 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo25);

		text = "Tools";
		ButtonMetaInfo btnMetaInfo26 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo26);

		text = "HTML5";
		ButtonMetaInfo btnMetaInfo27 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo27);

		text = "CSS3";
		ButtonMetaInfo btnMetaInfo28 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo28);

		text = "XML";
		ButtonMetaInfo btnMetaInfo29 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo29);

		text = "JS/AJAX";
		ButtonMetaInfo btnMetaInfo30 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo30);

		text = "Math";
		ButtonMetaInfo btnMetaInfo31 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo31);

		text = "Savvy";
		ButtonMetaInfo btnMetaInfo32 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo32);

		text = "Android";
		ButtonMetaInfo btnMetaInfo33 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo33);

		text = "Java EE";
		ButtonMetaInfo btnMetaInfo34 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo34);

		text = "JSF";
		ButtonMetaInfo btnMetaInfo35 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo35);

		text = "Hibernate";
		ButtonMetaInfo btnMetaInfo36 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo36);

		text = "Spring";
		ButtonMetaInfo btnMetaInfo37 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo37);

		text = "Modern OS";
		ButtonMetaInfo btnMetaInfo38 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo38);

		text = "Win Admin";
		ButtonMetaInfo btnMetaInfo39 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo39);

		text = "Java FX";
		ButtonMetaInfo btnMetaInfo40 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo40);

		text = "REGEXP";
		ButtonMetaInfo btnMetaInfo41 = new ButtonMetaInfo(text);
		btnMetaInfoList.add(btnMetaInfo41);

		// set ID to every button
		// ID = index of Button in the list
		int i = 0;
		for (ButtonMetaInfo buttonMetaInfo : btnMetaInfoList) {
			buttonMetaInfo.setID(i);
			i++;
		}
	}

	public List<ButtonMetaInfo> getList() {
		return btnMetaInfoList;
	}

	public ButtonMetaInfo getButtonMetaInfoByID(int ID) {
		return btnMetaInfoList.get(ID);
	}

	// TODO this list not used
	/*	private List<String> buttonText = new ArrayList<>();
		{
			// REPEAT
			buttonText.add("Yesterday Audio");
			buttonText.add("Mnemosyne NEW");
			buttonText.add("Mnemosyne OLD");
			buttonText.add("Cram / Important");
			buttonText.add("Конспекты");
			buttonText.add("Audio backlog");
			// STUDY
			buttonText.add("Own Project");
			buttonText.add("Patterns");
			buttonText.add("Prog Tasks");
			buttonText.add("JLS");
			buttonText.add("Core API");
			buttonText.add("Tutorials");
			buttonText.add("Certification");
			buttonText.add("Do Tests");
			buttonText.add("Hackerrank");
			buttonText.add("Interview Q&A");
			buttonText.add("Algo");
			buttonText.add("JUnit/Hamcrest");
			buttonText.add("Logging");
			buttonText.add("Mockito");
			buttonText.add("SQL");
			buttonText.add("Git");
			buttonText.add("Maven/Gradle");
			buttonText.add("Libs");
			buttonText.add("Linux");
			buttonText.add("Tools");
			buttonText.add("HTML5");
			buttonText.add("CSS3");
			buttonText.add("XML");
			buttonText.add("JS/AJAX");
			buttonText.add("Math");
			buttonText.add("Savvy");
			buttonText.add("Android");
			buttonText.add("Java EE");
			buttonText.add("JSF");
			buttonText.add("Hibernate");
			buttonText.add("Hibernate");
			buttonText.add("Spring");
			buttonText.add("Modern OS");
			buttonText.add("Win Admin");
		}
	*/
}
