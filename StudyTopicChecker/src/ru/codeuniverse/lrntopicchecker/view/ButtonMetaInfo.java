package ru.codeuniverse.lrntopicchecker.view;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains info about Button items:
 * <p>
 * 1) text inside Button
 * </p>
 * <p>
 * 2) importance of Button (the topic it represents)
 * </p>
 */
public class ButtonMetaInfo {

	private String buttonText;
	private String buttonToolTip; // TODO not needed
	private Importance buttonImportance;
	private List<String> links = new ArrayList<>();
	private int ID;

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public List<String> getLinks() {
		return new ArrayList<String>(this.links);
	}

	public void setLinks(List<String> links) {
		if (links == null) {
			return;
		} else {
			this.links.addAll(links);
		}
	}

	public ButtonMetaInfo(String buttonText, Importance importance) {
		setText(buttonText);
		setImportance(importance);
	}

	public ButtonMetaInfo(String buttonText) {
		setText(buttonText);
		setImportance(Importance.LOW);
	}

	public String getText() {
		return buttonText;
	}

	public void setText(String buttonText) {
		this.buttonText = buttonText;
	}

	public String getToolTip() {
		return buttonToolTip;
	}

	public void setToolTip(String buttonToolTip) {
		this.buttonToolTip = buttonToolTip;
	}

	public Importance getImportance() {
		return buttonImportance;
	}

	public void setImportance(Importance buttonImportance) {
		this.buttonImportance = buttonImportance;
	}

}
