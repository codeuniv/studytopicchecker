package ru.codeuniverse.lrntopicchecker.view;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ru.codeuniverse.lrntopicchecker.dao.TopicDataAccess;

public class FXMain extends Application {

	private static final Logger logger = LoggerFactory.getLogger(FXMain.class);

	static HostServices hostServices;

	private static TopicDataAccess topicDAO; // to close connection at app
												// shutdown

	public static void setTopicDAO(TopicDataAccess topicDAO) {
		FXMain.topicDAO = topicDAO;
	}

	public FXMain() {
		hostServices = getHostServices();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		Parent root = getMainSceneRoot();
		logger.info("root is " + root);
		Scene mainScene = new Scene(root);
		primaryStage.setScene(mainScene);
		primaryStage.setMaximized(true);
		setAppIcon(primaryStage);
		primaryStage.show();

	}

	private void setAppIcon(Stage primaryStage) {
		InputStream appIconImg = getClass()
				.getResourceAsStream("/circle_blue_checkmark1.png");
		Image appIcon = new Image(appIconImg);
		primaryStage.getIcons().add(appIcon);
	}

	private Parent getMainSceneRoot() {
		URL url = getClass().getResource("MainView.fxml");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(url);
		BorderPane root = null;
		try {
			root = (BorderPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return root;
	}

	@Override
	public void stop() throws Exception {
		topicDAO.close(); // close connection
		super.stop();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
