package ru.codeuniverse.lrntopicchecker.domain;

import java.util.ArrayList;
import java.util.List;

import ru.codeuniverse.lrntopicchecker.view.Importance;

public class Topic {

	private int topicNumber;
	private String topicName;
	private Importance topicImportance;
	private List<String> topicUrl = new ArrayList<>();

	public int getTopicNumber() {
		return topicNumber;
	}

	public void setTopicNumber(int topicNumber) {
		this.topicNumber = topicNumber;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public Importance getTopicImportance() {
		return topicImportance;
	}

	public void setTopicImportance(Importance topicImportance) {
		this.topicImportance = topicImportance;
	}

	public List<String> getTopicUrl() {
		return topicUrl;
	}

	public void setTopicUrl(List<String> topicUrl) {
		this.topicUrl = topicUrl;
	}

}
