package ru.codeuniverse.lrntopicchecker.dao;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.codeuniverse.lrntopicchecker.domain.Topic;
import ru.codeuniverse.lrntopicchecker.view.Importance;

//XXX Enable existing of folder and db file
//C:/ProgramData/StudyTopicChecker/studychecker.db
public class TopicDataAccess {

	private static final Logger logger = LoggerFactory.getLogger(TopicDataAccess.class);

	private Connection connection;
	private Statement statement;
	private Statement statement2;

	public TopicDataAccess() {
		connection = this.connect();
		try {
			statement = connection.createStatement();
			statement2 = connection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Connection connect() {
		Path dbFile = Paths.get("C:/ProgramData/StudyTopicChecker/studychecker.db");
		String connectionString = "jdbc:sqlite:" + dbFile;
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(connectionString);
			logger.info("Successfully connected to DB. Connection conn = " + conn);
		} catch (SQLException e) {
			logger.error("Can't connect to DB. Connection string:  " + connectionString);
			// printStackTrace is bad in production?
			// What shall be instead?
			e.printStackTrace();
			// shall I also rethrow exception with a better explanation of its
			// cause?
			// new SQLException("Can't connect to SQLite db. Connection string:
			// " + connectionString);
		}

		return conn;
	}

	public void close() throws SQLException {
		connection.close();
	}

	// returns number of Topics
	public int getTopicCount() {
		int topicsCount = 0;
		try {

			String sqlQuery = "SELECT COUNT(TopicName) FROM Topic;";
			ResultSet queryResult = statement.executeQuery(sqlQuery);
			topicsCount = queryResult.getInt(1);
			logger.debug("getTopicCount(): topicsCount = " + topicsCount);
		} catch (SQLException e) {
			logger.error("getTopicCount(): query db failed");
		}

		return topicsCount;
	}

	public List<String> getTopicLinks(int topicID) {

		//@formatter:off
		String queryString = 
				"SELECT Url "
			    + "FROM StudyAid "
			    + "WHERE TopicID = " + topicID 
			    + " ORDER BY StudyAidNumber;";
		//@formatter:on
		List<String> topicLinks = new ArrayList<String>();
		try {
			ResultSet queryResult = statement2.executeQuery(queryString);
			while (queryResult.next()) {
				topicLinks.add(queryResult.getString(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return topicLinks;
	}

	public Topic getTopicByID(int topicID) {

		return null;
	}

	public List<Topic> getAllTopics() throws SQLException {
		String enableFK = "PRAGMA foreign_keys = ON";
		try {
			statement.execute(enableFK);
		} catch (SQLException e) {
			throw new SQLException("enabling  foreign key query failed");
		}

		//@formatter:off
		String queryString = 
				"SELECT TopicNumber, TopicName, TopicImportance, TopicID "
				+ "FROM Topic "				
				+ "ORDER BY TopicNumber;";
		//@formatter:on
		List<Topic> topics = new ArrayList<>();
		ResultSet queryResult = statement.executeQuery(queryString);
		List<String> topicUrls = new ArrayList<>();
		while (queryResult.next()) {
			Topic topic = new Topic();
			topic.setTopicNumber(queryResult.getInt(1));
			topic.setTopicName(queryResult.getString(2));
			topic.setTopicImportance(Importance.values()[queryResult.getInt(3)]);

			topicUrls = getTopicLinks(queryResult.getInt("TopicID"));
			topic.setTopicUrl(topicUrls);
			topics.add(topic);
		}

		return topics;
	}

	// changes topicNumber in row with topicID in Topic table
	public void updateTopicNumber(int topicID, int topicNumber) {
		String updateString = "UPDATE Topic " + "SET TopicNumber = ? " + " WHERE TopicID = ? ";
		try (PreparedStatement prepStmt = connection.prepareStatement(updateString)) {
			connection.setAutoCommit(false);
			prepStmt.setInt(1, topicNumber);
			prepStmt.setInt(2, topicID);
			prepStmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			logger.error(
					"updateTopicNumber(int topicID, int topicNumber) failed with arguments: topicID = "
							+ topicID + ", topicNumber = " + topicNumber);
			e.printStackTrace();
		}
	}

	// changes topicName in row with topicID in Topic table
	public void updateTopicName(int topicID, String topicName) {
		String updateString = "UPDATE Topic " + "SET topicName = ? " + " WHERE TopicID = ? ";
		try (Connection conn = this.connect();
				PreparedStatement prepStmt = conn.prepareStatement(updateString)) {
			conn.setAutoCommit(false);
			prepStmt.setString(1, topicName);
			prepStmt.setInt(2, topicID);
			prepStmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			logger.error(
					"updateTopicName(int topicID, int topicName) failed with arguments: topicID = "
							+ topicID + ", topicName = " + topicName);
			e.printStackTrace();
		}
	}

	// changes TopicImportance in row with topicID in Topic table
	public void updateTopicImportance(int topicID, int topicImportance) {
		// check that Importance value is within acceptable range
		int maxValue = Importance.values().length - 1;
		if (topicImportance > maxValue) {
			throw new IllegalArgumentException("Such Importance value does not exist");
		}

		// access the database
		String updateString = "UPDATE Topic " + "SET TopicImportance = ? "
				+ " WHERE TopicID = ? ";
		try (Connection conn = this.connect();
				PreparedStatement prepStmt = conn.prepareStatement(updateString)) {
			conn.setAutoCommit(false);
			prepStmt.setInt(1, topicImportance);
			prepStmt.setInt(2, topicID);
			prepStmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			logger.error(
					"updateTopicImportance(int topicID, int TopicImportance) failed with arguments: topicID = "
							+ topicID + ", TopicImportance = " + topicImportance);
			e.printStackTrace();
		}
	}

	// adds URL to StudyAid table
	// this URL is a weblink or path to file with study materials
	// one Topic may have multiple URLs
	public void insertStudyAidURL(int topicID, String URL) {
		try (Connection conn = this.connect(); Statement stmt = conn.createStatement()) {

			conn.setAutoCommit(false);

			String enableFK = "PRAGMA foreign_keys = ON";
			stmt.executeUpdate(enableFK);

			//@formatter:off
			String quote = "\"";
			String comma = ",";
			String sqlInsertUrl = 
					"INSERT INTO StudyAid (TopicID, Url) " +
					 "VALUES(" + topicID + comma + quote + URL + quote + ")";
			//@formatter:on
			stmt.executeUpdate(sqlInsertUrl);
			conn.commit();
		} catch (SQLException e) {
			logger.error("addStudyAidURI(int TopicID, String URL) failed with arguments: "
					+ "topicID = " + topicID + ", " + "URL = " + URL);
		}
	}

	public static void main(String[] args) {
		TopicDataAccess topicDAO = new TopicDataAccess();
		List<Topic> topics = new ArrayList<>();
		try {
			topics = topicDAO.getAllTopics();
			topicDAO.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Topic topic : topics) {
			System.out.println(topic.getTopicNumber());
			System.out.println(topic.getTopicName());
			System.out.println(topic.getTopicImportance());
			System.out.println(topic.getTopicUrl());
			System.out.println("*************************88");

		}

		// System.out.println(topicDAO.getTopicCount());
		// System.out.println(topicDAO.getTopicLinks(1));

		// topicDAO.insertStudyAidURL_for_Topic(1, "http://www.yandex.ru");
		// for (int i = 1; i <= 41; i++) {
		// topicDAO.updateTopicNumber(i, i);
		// }

		// topicDAO.updateTopicImportance(3, 2); // no changes in DB
		// method below changes DB as intended
		// topicDAO.updateTopicNumber1(3, 777);
	}
}
